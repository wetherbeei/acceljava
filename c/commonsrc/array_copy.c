#include <inttypes.h>
#include <jni.h>
#include "array_copy.h"

/* Type conversion
 * jboolean uint8_t
 * jbyte int8_t
 * jchar uint16_t
 * jshort int16_t
 * jint int32_t
 * jlong int64_t
 * jfloat float
 * jdouble double
 */

int8_t* copy_byte_array(JNIEnv* env, jbyteArray b, jboolean* c) {
  return (int8_t*) (*env)->GetByteArrayElements(env, b, c);
}
uint8_t* copy_boolean_array(JNIEnv* env, jbooleanArray b, jboolean* c) {
  return (uint8_t*) (*env)->GetBooleanArrayElements(env, b, c);
}
uint16_t* copy_char_array(JNIEnv* env, jcharArray b, jboolean* c) {
  return (uint16_t*) (*env)->GetCharArrayElements(env, b, c);
}
int16_t* copy_short_array(JNIEnv* env, jshortArray b, jboolean* c) {
  return (int16_t*) (*env)->GetShortArrayElements(env, b, c);
}
int32_t* copy_int_array(JNIEnv* env, jintArray b, jboolean* c) {
  return (int32_t*) (*env)->GetIntArrayElements(env, b, c);
}
int64_t* copy_long_array(JNIEnv* env, jlongArray b, jboolean* c) {
  return (int64_t*) (*env)->GetLongArrayElements(env, b, c);
}
float* copy_float_array(JNIEnv* env, jfloatArray b, jboolean* c) {
  return (float*) (*env)->GetFloatArrayElements(env, b, c);
}
double* copy_double_array(JNIEnv* env, jdoubleArray b, jboolean* c) {
  return (double*) (*env)->GetDoubleArrayElements(env, b, c);
}

void free_byte_array(JNIEnv* env, jbyteArray b, int8_t* c) {
  (*env)->ReleaseByteArrayElements(env, b, (jbyte*) c, 0);
}
void free_boolean_array(JNIEnv* env, jbooleanArray b, uint8_t* c) {
  (*env)->ReleaseBooleanArrayElements(env, b, (jboolean*) c, 0);
}
void free_char_array(JNIEnv* env, jcharArray b, uint16_t* c) {
  (*env)->ReleaseCharArrayElements(env, b, (jchar*) c, 0);
}
void free_short_array(JNIEnv* env, jshortArray b, int16_t* c) {
  (*env)->ReleaseShortArrayElements(env, b, (jshort*) c, 0);
}
void free_int_array(JNIEnv* env, jintArray b, int32_t* c) {
  (*env)->ReleaseIntArrayElements(env, b, (jint*) c, 0);
}
void free_long_array(JNIEnv* env, jlongArray b, int64_t* c) {
  (*env)->ReleaseLongArrayElements(env, b, (jlong*) c, 0);
}
void free_float_array(JNIEnv* env, jfloatArray b, float* c) {
  (*env)->ReleaseFloatArrayElements(env, b, (jfloat*) c, 0);
}
void free_double_array(JNIEnv* env, jdoubleArray b, double* c) {
  (*env)->ReleaseDoubleArrayElements(env, b, (jdouble*) c, 0);
}