#include <inttypes.h>
#include <jni.h>
#include "field_copy.h"

int8_t get_byte_field(JNIEnv* env, jobject a, jfieldID b) {
	return (int8_t) (*env)->GetByteField(env, a, b);
}
uint8_t get_boolean_field(JNIEnv* env, jobject a, jfieldID b) {
	return (uint8_t) (*env)->GetBooleanField(env, a, b);
}
uint16_t get_char_field(JNIEnv* env, jobject a, jfieldID b) {
	return (uint16_t) (*env)->GetCharField(env, a, b);
}
int16_t get_short_field(JNIEnv* env, jobject a, jfieldID b) {
	return (int16_t) (*env)->GetShortField(env, a, b);
}
int32_t get_int_field(JNIEnv* env, jobject a, jfieldID b) {
	return (int32_t) (*env)->GetIntField(env, a, b);
}
int64_t get_long_field(JNIEnv* env, jobject a, jfieldID b) {
	return (int64_t) (*env)->GetLongField(env, a, b);
}
float get_float_field(JNIEnv* env, jobject a, jfieldID b) {
	return (float) (*env)->GetFloatField(env, a, b);
}
double get_double_field(JNIEnv* env, jobject a, jfieldID b) {
	return (double) (*env)->GetDoubleField(env, a, b);
}

void set_byte_field(JNIEnv* env, jobject a, jfieldID b, int8_t c) {
	(*env)->SetByteField(env, a, b, c);
}
void set_boolean_field(JNIEnv* env, jobject a, jfieldID b, uint8_t c) {
	(*env)->SetBooleanField(env, a, b, c);
}
void set_char_field(JNIEnv* env, jobject a, jfieldID b, uint16_t c) {
	(*env)->SetCharField(env, a, b, c);
}
void set_short_field(JNIEnv* env, jobject a, jfieldID b, int16_t c) {
	(*env)->SetShortField(env, a, b, c);
}
void set_int_field(JNIEnv* env, jobject a, jfieldID b, int32_t c) {
	(*env)->SetIntField(env, a, b, c);
}
void set_long_field(JNIEnv* env, jobject a, jfieldID b, int64_t c) {
	(*env)->SetLongField(env, a, b, c);
}
void set_float_field(JNIEnv* env, jobject a, jfieldID b, float c) {
	(*env)->SetFloatField(env, a, b, c);
}
void set_double_field(JNIEnv* env, jobject a, jfieldID b, double c) {
	(*env)->SetDoubleField(env, a, b, c);
}