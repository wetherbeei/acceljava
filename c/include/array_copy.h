#ifndef _ARRAY_COPY_H
#define _ARRAY_COPY_H
#include <inttypes.h>
#include <jni.h>

/* Array copy for boolean, char, byte, short, int, float, double */
int8_t* copy_byte_array(JNIEnv* env, jbyteArray b, jboolean* c);
uint8_t* copy_boolean_array(JNIEnv* env, jbooleanArray b, jboolean* c);
uint16_t* copy_char_array(JNIEnv* env, jcharArray b, jboolean* c);
int16_t* copy_short_array(JNIEnv* env, jshortArray b, jboolean* c);
int32_t* copy_int_array(JNIEnv* env, jintArray b, jboolean* c);
int64_t* copy_long_array(JNIEnv* env, jlongArray b, jboolean* c);
float* copy_float_array(JNIEnv* env, jfloatArray b, jboolean* c);
double* copy_double_array(JNIEnv* env, jdoubleArray b, jboolean* c);

void free_byte_array(JNIEnv* env, jbyteArray b, int8_t* c);
void free_boolean_array(JNIEnv* env, jbooleanArray b, uint8_t* c);
void free_char_array(JNIEnv* env, jcharArray b, uint16_t* c);
void free_short_array(JNIEnv* env, jshortArray b, int16_t* c);
void free_int_array(JNIEnv* env, jintArray b, int32_t* c);
void free_long_array(JNIEnv* env, jlongArray b, int64_t* c);
void free_float_array(JNIEnv* env, jfloatArray b, float* c);
void free_double_array(JNIEnv* env, jdoubleArray b, double* c);

#endif
