#include <inttypes.h>
#include <jni.h>

#ifndef _LAUNCH_H
#define _LAUNCH_H

uint32_t get_block_dim(jobject this);
uint32_t get_thread_dim(jobject this);

#endif