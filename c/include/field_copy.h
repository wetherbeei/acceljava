#ifndef _FIELD_COPY_H
#define _FIELD_COPY_H
#include <inttypes.h>
#include <jni.h>

int8_t get_byte_field(JNIEnv* env, jobject a, jfieldID b);
uint8_t get_boolean_field(JNIEnv* env, jobject a, jfieldID b);
uint16_t get_char_field(JNIEnv* env, jobject a, jfieldID b);
int16_t get_short_field(JNIEnv* env, jobject a, jfieldID b);
int32_t get_int_field(JNIEnv* env, jobject a, jfieldID b);
int64_t get_long_field(JNIEnv* env, jobject a, jfieldID b);
float get_float_field(JNIEnv* env, jobject a, jfieldID b);
double get_double_field(JNIEnv* env, jobject a, jfieldID b);

void set_byte_field(JNIEnv* env, jobject a, jfieldID b, int8_t c);
void set_boolean_field(JNIEnv* env, jobject a, jfieldID b, uint8_t c);
void set_char_field(JNIEnv* env, jobject a, jfieldID b, uint16_t c);
void set_short_field(JNIEnv* env, jobject a, jfieldID b, int16_t c);
void set_int_field(JNIEnv* env, jobject a, jfieldID b, int32_t c);
void set_long_field(JNIEnv* env, jobject a, jfieldID b, int64_t c);
void set_float_field(JNIEnv* env, jobject a, jfieldID b, float c);
void set_double_field(JNIEnv* env, jobject a, jfieldID b, double c);
#endif