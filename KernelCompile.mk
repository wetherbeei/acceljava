KERNELSRC := $(wildcard $(JNIPATH)*.c)
KERNELSO := $(patsubst %.c,%.so,$(KERNELSRC))
CINCLUDE := -I$(SCRIPT_DIR)/c/include
# TODO: Remove this, make the user put JNI headers on their default path
JNIINCLUDE := -I${JAVA_HOME}/include -I${JAVA_HOME}/include/linux
COMMONSRC := $(wildcard $(SCRIPT_DIR)/c/commonsrc/*.c)

native : $(KERNELSO)

# $@ is the target, $^ is the dependency
%.so : %.c $(COMMONSRC)
	$(CC) -shared -fPIC -Wl,-soname,lib$(@).1.0 $(shell cat $(@).libs) $(JNIINCLUDE) $(CINCLUDE) -o $@ $^