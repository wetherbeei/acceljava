package edu.illinois.crhc.acceljava;

/**
 * Specify an external library and header files to be included in the compilation for this module.
 */
public @interface Include {
  String[] libs();
  String[] headers();
}
