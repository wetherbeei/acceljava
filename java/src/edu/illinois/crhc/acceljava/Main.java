package edu.illinois.crhc.acceljava;

import java.io.File;
import java.io.FileReader;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Main {
  /**
   * @param args
   */
  public static void main(String[] args) {
    if (args.length != 3) {
      System.out.println("Usage: [mode] [/rootPath] [/scriptDir]");
      return;
    }
    final String mode = args[0];
    final String rootPath = args[1];
    final String scriptDir = args[2];
    final String classPath = rootPath + "/src/";
    final String outputPath = rootPath + "/gen/";
    final String jniPath = rootPath + "/jni/";
    
    // Create directories
    new File(outputPath).mkdirs();
    new File(jniPath).mkdirs();
    
    final ASTParser parser = new ASTParser();
    
    Path start = FileSystems.getDefault().getPath(classPath);
    try {
      // Populate class table
      Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
          String path = file.toString();
          if (path.endsWith(".java")) {
            parser.populateClassDefinitions(path);
          }
          return FileVisitResult.CONTINUE;
        }
      });

      // Parse
      final List<Module> modules = new ArrayList<Module>();
      
      Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
          String path = file.toString();
          if (path.endsWith(".java")) {
            System.out.println("Parsing file " + path);
            List<Module> m = parser.parseFile(path);
            if (m != null)
              modules.addAll(m);
          }
          return FileVisitResult.CONTINUE;
        }
      });
      
      Writer.writeJni(modules, jniPath);
      if (mode.equals("native")) {
        Writer.writeExternals(modules, jniPath);
      } else if (mode.equals("android")) {
        String androidMk = jniPath + "Android.mk";
        // Overwrite an existing generated file, but don't overwrite a custom makefile
        File f = new File(androidMk);
        char cbuf[] = new char[Writer.GENERATED_HEADER.length() + 1];
        if (f.exists()) {
          new FileReader(f).read(cbuf, 0, Writer.GENERATED_HEADER.length() + 1);
          if (!new String(cbuf).equals("#" + Writer.GENERATED_HEADER)) {
            androidMk = jniPath + "AccelJava.android.mk";
          }
        }
        Writer.writeAndroidMk(modules, androidMk, scriptDir);
      } else {
        throw new Exception("Unknown mode " + mode);
      }
    } catch (Exception e) {
      System.err.println("Error!");
      e.printStackTrace();
    }
  }
}
