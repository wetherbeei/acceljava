package edu.illinois.crhc.acceljava;

public class ParallelRuntime {
  private static ParallelRuntime instance;
  
  private ParallelRuntime() {
    
  }
  
  public static ParallelRuntime get() {
    if (instance == null) {
      instance = new ParallelRuntime();
    }
    return instance;
  }
  
  
}
