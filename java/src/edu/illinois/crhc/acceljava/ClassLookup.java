package edu.illinois.crhc.acceljava;

import com.sun.source.tree.ClassTree;

class ClassLookup {
  ClassTree tree;
  String qualifiedType;

  ClassLookup(ClassTree tree, String qualifiedType) {
    this.tree = tree;
    this.qualifiedType = qualifiedType;
  }
}