package edu.illinois.crhc.acceljava;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.lang.model.util.Elements;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import com.sun.source.tree.AnnotationTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ImportTree;
import com.sun.source.tree.LineMap;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.SourcePositions;
import com.sun.source.util.TreeScanner;
import com.sun.source.util.Trees;

/**
 * Parse all Java classes for accelerated modules, then rewrite these classes to a different
 * directory.
 */
public class ASTParser {

  private class FunctionProcessor extends TreeScanner<Void, Void> {
    ArrayList<Function> funcs;

    public FunctionProcessor(ArrayList<Function> funcs) {
      this.funcs = funcs;
    }

    @Override
    public Void visitMethod(MethodTree node, Void p) {
      if (!node.getName().toString().equals("<init>")
          && node.getModifiers().toString().contains("native")) {
        Function func = new Function();
        func.lineNumberStart =
            (int) lineMap.getLineNumber(sourcePositions.getStartPosition(currentTree, node));
        func.name = node.getName().toString();
        FunctionArg returnType = new FunctionArg();
        returnType.setType(node.getReturnType());
        func.returnType = returnType;
        func.isPublic = (node.getModifiers().toString().contains("public"));
        /*
         * Identifier (string) -> Types.getDeclaredType -> TypeMirror -(cast)> DeclaredType ->
         * Element (class) -> lookup fields
         */
        
        for (VariableTree var : node.getParameters()) {
          FunctionArg arg = new FunctionArg(var);
          func.arguments.add(arg);
        }
        funcs.add(func);
      }
      return super.visitMethod(node, p);
    }
  }

  /**
   * Finds all XYZ extends AccelKernel definitions.
   */
  private class ModuleSubclassProcessor extends TreeScanner<Void, String> {

    public ModuleSubclassProcessor() {

    }

    @Override
    public Void visitClass(ClassTree node, String packagePrefix) {
      Tree extendsClause = node.getExtendsClause();

      if (extendsClause != null) {
        if (extendsClause.toString().equals("AccelModule")) {
          System.out.println("Found kernel class: " + node.getSimpleName());
          Module module = new Module();
          module.name = currentPackage + "." + packagePrefix + node.getSimpleName();
          module.qualifiedType =
              currentPackage.replace(".", "/") + "/"
                  + (packagePrefix + node.getSimpleName()).replace(".", "$");
          module.pack = currentPackage;
          module.scope = packagePrefix + node.getSimpleName();
          module.lineNumberStart =
              (int) lineMap.getLineNumber(sourcePositions.getStartPosition(currentTree, node));
          for(AnnotationTree t : node.getModifiers().getAnnotations()) {
            if (t.getAnnotationType().toString().equals("Include")) {              
              for (ExpressionTree a : t.getArguments()) {
                List<String> values = new ArrayList<String>();
                String arg = a.toString();
                int headerStart = 0, headerEnd = 0;
                while (true) {
                  headerStart = arg.indexOf("\"", headerEnd+1);
                  if (headerStart < 0) {
                    break;
                  }
                  headerEnd = arg.indexOf("\"", headerStart+1);
                  values.add(arg.substring(headerStart+1, headerEnd));
                }
                
                if (arg.startsWith("libs")) {
                  if (module.libs == null) {
                    module.libs = new ArrayList<String>();
                  }
                  module.libs.addAll(values);
                } else if (arg.startsWith("headers")) {
                  if (module.headers == null) {
                    module.headers = new ArrayList<String>();
                  }
                  module.headers.addAll(values);
                } 
              }
            }
          }
          for (Tree t : node.getMembers()) {
            t.accept(new FunctionProcessor(module.functions), null);
          }

          // Pull out kernel bodies
          List<String> lines = null;
          try {
            lines = readFile(currentFile);
            for (Function function : module.functions) {
              StringBuilder kernelBody = new StringBuilder();
              String line = lines.get(function.lineNumberStart - 1);
              int start = line.indexOf("/*-{");
              kernelBody.append(line.substring(start + 3) + "\n");
              for (int lineNumber = function.lineNumberStart; lineNumber < lines.size(); lineNumber++) {
                line = lines.get(lineNumber);
                if (line.contains("}-*/")) {
                  // Found end of kernel block.
                  kernelBody.append(line.substring(0, line.indexOf("}-*/") + 1) + "\n");
                  function.lineNumberEnd = lineNumber + 1;
                  break;
                } else {
                  kernelBody.append(line + "\n");
                }
              }
              if (function.lineNumberEnd == 0) {
                System.err.println("End of kernel '}-*/' not found for function " + function.name
                    + " on line " + function.lineNumberStart + " in file " + currentFile);
              }
              function.body = kernelBody.toString();
            }
          } catch (IOException e) {
            e.printStackTrace();
          }

          // Fill in function argument classes
          for (Function f : module.functions) {
            for (FunctionArg a : f.arguments) {
              if (!a.isPrimitiveType()) {
                ClassLookup clazz = lookupClass(currentPackageTree, module.scope, a.type);
                if (clazz == null) {
                  System.out.println("ERROR: Class not found! " + a.type + " "
                      + currentPackageTree.getPackageName());
                  System.exit(1);
                }
                a.qualifiedType = clazz.qualifiedType;
                module.addClassMirror(clazz, a.type);
              }
            }
            // Return type
            if (!f.returnType.isPrimitiveType()) {
              ClassLookup clazz =
                  lookupClass(currentPackageTree, module.scope, f.returnType.type);
              if (clazz == null) {
                System.out.println("ERROR: Class not found! " + f.returnType.type + " "
                    + currentPackageTree.getPackageName());
                System.exit(1);
              }
              f.returnType.qualifiedType = clazz.qualifiedType;
              module.addClassMirror(clazz, f.returnType.type);
            }
          }
          currentModules.add(module);
        }
      }
      return super.visitClass(node, packagePrefix + node.getSimpleName() + ".");
    }
  }

  class PackageVisitor extends TreeScanner<Void, Void> {
    @Override
    public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
      currentPackage = "";
      ExpressionTree packageName = node.getPackageName();
      currentPackageTree = node;
      if (packageName != null) {
        String packageNameString = String.valueOf(packageName);
        if (packageNameString.length() > 0) {
          currentPackage = packageNameString;
        }
      }
      TreeScanner<Void, String> visitor = new ModuleSubclassProcessor();
      for (Tree decls : node.getTypeDecls()) {
        decls.accept(visitor, "");
      }
      return super.visitCompilationUnit(node, p);
    }
  }

  JavaCompiler compiler;
  DiagnosticCollector<JavaFileObject> diagnosticsCollector;
  StandardJavaFileManager fileManager;
  SourcePositions sourcePositions;
  Iterable<? extends CompilationUnitTree> parseResult;
  LineMap lineMap;
  /*
   * These change with every compilation tree, but stash references here to avoid passing them as
   * arguments.
   */
  String currentFile;
  Elements currentElements;
  CompilationUnitTree currentTree;
  List<Module> currentModules;
  HashMap<String, HashMap<String, ClassTree>> classDefinitions;
  String currentPackage;
  CompilationUnitTree currentPackageTree;

  public ASTParser() {
    compiler = ToolProvider.getSystemJavaCompiler();
    diagnosticsCollector = new DiagnosticCollector<JavaFileObject>();
    fileManager = compiler.getStandardFileManager(diagnosticsCollector, null, null);
    classDefinitions = new HashMap<String, HashMap<String, ClassTree>>();
  }

  public List<Module> parseFile(String path) {
    Iterable<? extends JavaFileObject> files = fileManager.getJavaFileObjects(path);
    CompilationTask task =
        compiler.getTask(null, fileManager, diagnosticsCollector, null, null, files);

    final JavacTask javacTask = (JavacTask) task;
    sourcePositions = Trees.instance(javacTask).getSourcePositions();
    parseResult = null;
    try {
      parseResult = javacTask.parse();
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }

    currentFile = path;
    currentModules = new ArrayList<Module>();

    for (CompilationUnitTree tree : parseResult) {
      lineMap = tree.getLineMap();
      currentTree = tree;
      currentElements = javacTask.getElements();
      tree.accept(new PackageVisitor(), null);
    }

    return currentModules;
  }

  public void populateClassDefinitions(String path) {
    Iterable<? extends JavaFileObject> files = fileManager.getJavaFileObjects(path);
    CompilationTask task =
        compiler.getTask(null, fileManager, diagnosticsCollector, null, null, files);
    final JavacTask javacTask = (JavacTask) task;
    parseResult = null;
    try {
      parseResult = javacTask.parse();
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }
    for (CompilationUnitTree tree : parseResult) {
      tree.accept(new TreeScanner<Void, Void>() {
        @Override
        public Void visitCompilationUnit(CompilationUnitTree node, Void p) {
          currentPackage = "";
          ExpressionTree packageName = node.getPackageName();
          if (packageName != null) {
            String packageNameString = String.valueOf(packageName);
            if (packageNameString.length() > 0) {
              currentPackage = packageNameString;
            }
          }
          TreeScanner<Void, String> visitor = new TreeScanner<Void, String>() {
            @Override
            public Void visitClass(ClassTree node, String packagePrefix) {
              if (classDefinitions.get(currentPackage) == null) {
                classDefinitions.put(currentPackage, new HashMap<String, ClassTree>());
              }
              classDefinitions.get(currentPackage).put(packagePrefix + node.getSimpleName(), node);
              return super.visitClass(node, packagePrefix + node.getSimpleName() + ".");
            }
          };
          for (Tree decls : node.getTypeDecls()) {
            decls.accept(visitor, "");
          }
          return super.visitCompilationUnit(node, p);
        }
      }, null);
    }
  }

  /**
   * Lookup the definition of a class.
   * 
   * Lookup order: 1. Search in the current file: within the current class scope upwards to the
   * root. 2. Search laterally across files with the same package value for implicitly included
   * classes. 3. Check all import statements for an exact endsWith match.
   * 
   * @param pack
   *          Current package eg "edu.illinois.crhc.acceljava"
   * @param scope
   *          Current scope eg "Test.InnerClass"
   * @param identifier
   *          The class name to search for
   * @return ClassTree the definition of this class if found
   */
  ClassLookup lookupClass(CompilationUnitTree packTree, String scope, String identifier) {
    String pack = packTree.getPackageName().toString();
    // Search nested scope and within same package
    HashMap<String, ClassTree> packClasses = classDefinitions.get(pack);
    if (packClasses != null) {
      String[] scopeWalk = scope.split("\\.");
      for (int i = scopeWalk.length; i >= 0; i--) {
        StringBuilder scopeTest = new StringBuilder();
        for (int j = 0; j < i; j++) {
          scopeTest.append(scopeWalk[j] + ".");
        }
        scopeTest.append(identifier);
        if (packClasses.containsKey(scopeTest.toString())) {
          return new ClassLookup(packClasses.get(scopeTest.toString()), pack.replace(".", "/")
              + "/" + scopeTest.toString().replace(".", "$"));
        }
      }
    }
    /*
     * Check if fully-qualified identifier (foo.bar.Widget) is used. This needs to search all
     * combinations of package and class nesting.
     */
    StringBuilder packTest = new StringBuilder();
    String[] qualifiedName = identifier.split("\\.");
    for (int i = 0; i < qualifiedName.length - 1; i++) {
      packTest.append(qualifiedName[i]);
      if (i != qualifiedName.length - 2) {
        packTest.append(".");
      }
    }
    String clazz = qualifiedName[qualifiedName.length - 1];
    if (classDefinitions.containsKey(packTest.toString())) {
      HashMap<String, ClassTree> foundPack = classDefinitions.get(packTest.toString());
      if (foundPack.containsKey(clazz)) {
        return new ClassLookup(foundPack.get(clazz), packTest.toString().replace(".", "/") + "/"
            + clazz.replace(".", "$"));
      }
    }

    /*
     * Search import statements. Last identifier segment must be class name. Search all of the
     * packages for the identifier by splitting off the class name. a.b.c.Tree Tree.Branch
     * Tree.Branch.Leaf
     */
    for (ImportTree imp : packTree.getImports()) {
      pack = imp.getQualifiedIdentifier().toString();
      String[] importName = pack.split("\\.");
      // Split off class name.
      // TODO: (edge case) no package
      StringBuilder importTest = new StringBuilder();
      for (int i = 0; i < importName.length - 1; i++) {
        importTest.append(importName[i]);
        if (i != importName.length - 2) {
          importTest.append(".");
        }
      }
      
      // See if the last import segment is * or matches the first segment of the identifier.
      if (classDefinitions.containsKey(importTest.toString())) {
        String identifierImport;
        String[] identifierParts = identifier.split(".");
        if (identifierParts.length == 0) {
          identifierImport = identifier;
        } else {
          identifierImport = identifierParts[0];
        }
        String importClass = importName[importName.length - 1];
        if (importClass.equals("*") || identifierImport.equals(importClass)) {
          HashMap<String, ClassTree> foundPack = classDefinitions.get(importTest.toString());
          if (foundPack.containsKey(identifier)) {
            return new ClassLookup(foundPack.get(identifier), importTest.toString().replace(".",
                "/")
                + "/" + identifier.replace(".", "$"));
          }
        }
      }
    }

    return null;
  }

  public void dumpClassTable() {
    for (String pack : classDefinitions.keySet()) {
      HashMap<String, ClassTree> packSet = classDefinitions.get(pack);
      for (String prefix : packSet.keySet()) {
        System.out.println(pack + " - " + prefix);
      }
    }
  }

  public List<String> readFile(String filename) throws IOException {
    FileReader fileReader = new FileReader(filename);
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    List<String> lines = new ArrayList<String>();
    String line = null;
    while ((line = bufferedReader.readLine()) != null) {
      lines.add(line);
    }
    bufferedReader.close();
    return lines;
  }
}
