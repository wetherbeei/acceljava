/*
 * Native C kernels.
 *
 * $className
 */

\#include <stdlib.h>
\#include <jni.h>
\#include "native.h"

// Custom headers
#foreach($header in $headers)
\#include "${header}"
#end

// Custom class structures
#foreach($clazz in $classes)
typedef struct {
#foreach($member in $clazz.members)
  $member.generateCType() #if ($member.isArray()) * #end $member.name ;
#end
} $clazz.name;

void get_${clazz.name}(JNIEnv* env, jobject obj_j, ${clazz.name}* obj_c) {
  jfieldID field_id;
  jclass clazz = (*env)->GetObjectClass(env, obj_j);
#foreach($member in $clazz.members)
  field_id = (*env)->GetFieldID(env, clazz, "${member.name}", "${member.generateJSignature()}");
  obj_c->${member.name} = get_${member.type}_field(env, obj_j, field_id);
#end
}

${clazz.name} * copy_${clazz.name}_array(JNIEnv* env, jobjectArray a, jboolean* wasCopy) {
  // TODO: implement persistent array check here (obj_j instanceof AccelArray)
  // TODO: cache field lookups
  int i;
  jsize len = (*env)->GetArrayLength(env, a);
  ${clazz.name}* obj_c = (${clazz.name}*) malloc(len * sizeof(${clazz.name}));
  for (i = 0; i < len; i++) {
    jobject obj_j = (*env)->GetObjectArrayElement(env, a, i);
    get_${clazz.name}(env, obj_j, &obj_c[i]);
    (*env)->DeleteLocalRef(env, obj_j);
  }
  return obj_c;
}

jobject create_${clazz.name}(JNIEnv* env) {
  jclass clazz = (*env)->FindClass(env, "${clazz.getQualifiedType()}");
  return (*env)->AllocObject(env, clazz);
}

void set_${clazz.name}(JNIEnv* env, jobject obj_j, ${clazz.name}* obj_c) {
  jfieldID field_id;
  jclass clazz = (*env)->GetObjectClass(env, obj_j);
#foreach($member in $clazz.members)
  field_id = (*env)->GetFieldID(env, clazz, "${member.name}", "${member.generateJSignature()}");
  set_${member.type}_field(env, obj_j, field_id, obj_c->${member.name});
#end
}

void free_${clazz.name}_array(JNIEnv* env, jobjectArray a, ${clazz.name} * obj_c) {
  // TODO: implement persistent array check here (obj_j instanceof AccelArray)
  // TODO: cache field lookups
  int i;
  jsize len = (*env)->GetArrayLength(env, a);
  for (i = 0; i < len; i++) {
    jobject obj_j = (*env)->GetObjectArrayElement(env, a, i);
    set_${clazz.name}(env, obj_j, &obj_c[i]);
    (*env)->DeleteLocalRef(env, obj_j);
  }
  free(obj_c);
}
#end

// Early defines
#foreach($method in $methods)
${method.returnType.generateCType()} #if ($method.returnType.isArray()) * #end ${method.name}(
  #foreach($arg in $method.arguments)
$arg.generateCType() #if ($arg.isArray() || !$arg.isPrimitiveType()) * #end ${arg.name}_c #if($foreach.count != $method.arguments.size()),#end
  #end
);
#end

#foreach($method in $methods)
#if($method.isPublic())
JNIEXPORT ${method.returnType.generateJNIType()} JNICALL AccelJava_${method.name}_launch(
  JNIEnv* env, jobject this
  #foreach($arg in $method.arguments)
    , $arg.generateJNIType() ${arg.name}_j
  #end
) {
  
  // Defines
  #foreach($arg in $method.arguments)
    $arg.generateCType() #if ($arg.isArray()) * #end ${arg.name}_c;
  #end
  
  // Setup/copy
  jboolean was_copy;
  #foreach($arg in $method.arguments)
    #if ($arg.isArray())
    ${arg.name}_c = copy_${arg.type}_array(env, ${arg.name}_j, &was_copy); // Array copy
    #elseif (!$arg.isPrimitiveType())
    get_${arg.generateCType()}(env, ${arg.name}_j, &${arg.name}_c); // Class copy
    #else
    ${arg.name}_c = ${arg.name}_j; // Data copy
    #end
  #end
  
  // Return value will not be an array
  #if (!$method.returnType.isVoid())
  ${method.returnType.generateCType()} retVal = 
  #end ${method.name}(
    #foreach($arg in $method.arguments)
    #if (!$arg.isArray() && !$arg.isPrimitiveType()) &#end ${arg.name}_c #if($foreach.count != $method.arguments.size()),#end
    #end
  );
  
  // Teardown
  #foreach($arg in $method.arguments)
    #if (!$arg.isConstant())
    #if ($arg.isArray())
    #if ($arg.isPrimitiveType())
    free_${arg.type}_array(env, ${arg.name}_j, ${arg.name}_c);
    #else
    free_${arg.generateCType()}_array(env, ${arg.name}_c, ${arg.name}_j);
    #end
    #elseif (!$arg.isPrimitiveType())
    set_${arg.generateCType()}(env, ${arg.name}_j, &${arg.name}_c);
    #end
    #end
  #end
  
  // Convert return value
  #if (!$method.returnType.isVoid())
  ${method.returnType.generateJNIType()} retJNIVal;
  #if ($method.returnType.isPrimitiveType())
    retJNIVal = retVal; // Single value
  #else
    // Class construction
    retJNIVal = create_${method.returnType.generateCType()}(env);
    set_${method.returnType.generateCType()}(env, retJNIVal, &retVal);
  #end
  return retJNIVal;
  #end
}
#end

${method.returnType.generateCType()} #if ($method.returnType.isArray()) * #end ${method.name}(
  #foreach($arg in $method.arguments)
    $arg.generateCType() #if ($arg.isArray() || !$arg.isPrimitiveType()) * #end $arg.name #if($foreach.count != $method.arguments.size()),#end
  #end
  )
  $method.body
#end

static JNINativeMethod methods[] = {
  /* name, sig, ptr */
  #set($pubMethods = 0)
  #foreach($method in $methods)
  #if ($method.isPublic())
  #set($pubMethods = $pubMethods + 1)
  {"${method.name}", "$method.generateSignature()", (void*)&AccelJava_${method.name}_launch},
  #end
  #end
};

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
  JNIEnv* env;
  jclass clazz;
  
  if ((*vm)->GetEnv(vm, (void**)&env, JNI_VERSION_1_4) != JNI_OK) {
    return -1;
  }
  
  clazz = (*env)->FindClass(env, "$classIdentifier");
  if (clazz == NULL) {
    return -1;
  }
  
  if ((*env)->RegisterNatives(env, clazz, methods, $pubMethods) < 0) {
    return -1;
  }
  
  return JNI_VERSION_1_4;
}