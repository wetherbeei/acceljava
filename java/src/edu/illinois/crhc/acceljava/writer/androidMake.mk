#${gen_header}
SCRIPT_DIR := ${script_dir}
COMMON_INCLUDES := $(SCRIPT_DIR)/c/include

LOCAL_PATH := $(call my-dir)

#foreach($module in $modules)
  include $(CLEAR_VARS)
  
  LOCAL_MODULE := ${module.getName()}
  LOCAL_SRC_FILES := ${module.getName()}.c
  LOCAL_C_INCLUDES := $(COMMON_INCLUDES)
  LOCAL_STATIC_LIBRARIES := AccelJava
  LOCAL_LDLIBS := #foreach($l in $module.libs) -l$(l) #end
  
  include $(BUILD_SHARED_LIBRARY)

#end

include $(SCRIPT_DIR)/c/Android.mk