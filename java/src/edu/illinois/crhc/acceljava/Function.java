package edu.illinois.crhc.acceljava;

import java.util.ArrayList;

/**
 * CudaKernels function token.
 */
public class Function {
  public String getName() {
    return name;
  }

  public ArrayList<FunctionArg> getArguments() {
    return arguments;
  }

  public FunctionArg getReturnType() {
    return returnType;
  }

  public void setArguments(ArrayList<FunctionArg> arguments) {
    this.arguments = arguments;
  }

  public boolean isPublic() {
    return isPublic;
  }

  public String getBody() {
    return body;
  }

  public int lineNumberStart, lineNumberEnd;
  public FunctionArg returnType;
  public String name;
  public ArrayList<FunctionArg> arguments = new ArrayList<FunctionArg>();
  public boolean isPublic;
  public String body;

  public String generateSignature() {
    StringBuilder s = new StringBuilder();
    s.append("(");

    for (FunctionArg arg : arguments) {
      s.append(arg.generateJSignature());
    }
    s.append(")");
    s.append(returnType.generateJSignature());
    return s.toString();
  }
}