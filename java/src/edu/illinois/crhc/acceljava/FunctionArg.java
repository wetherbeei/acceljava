package edu.illinois.crhc.acceljava;

import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;

public class FunctionArg {
  public FunctionArg() {
  }

  public FunctionArg(VariableTree var) {
    isConstant = var.getModifiers().toString().contains("final");
    name = var.getName().toString();
    setType(var.getType());
  }

  public void setType(Tree t) {
    type = t.toString();
    isArray = type.contains("[]");
    type = type.replace("[]", "");
    isPrimitive =
        (type.equals("boolean") || type.equals("byte") || type.equals("char")
            || type.equals("char") || type.equals("short") || type.equals("int")
            || type.equals("long") || type.equals("float") || type.equals("double") || type
            .equals("void"));
    isVoid = type.equals("void");
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getQualifiedType() {
    return qualifiedType;
  }

  public boolean isConstant() {
    return isConstant;
  }

  public boolean isArray() {
    return isArray;
  }

  public boolean isVoid() {
    return isVoid;
  }

  public String type; // Outer.Inner
  public String name;
  public String qualifiedType; // a/b/c/Outer$Inner
  public boolean isConstant;
  public boolean isArray;
  public boolean isPrimitive;
  public boolean isVoid;

  public String generateJNIType() {
    if (isVoid)
      return "void"; // No JNI void type
    StringBuilder s = new StringBuilder();
    s.append("j");
    if (isPrimitiveType()) {
      s.append(type);
    } else {
      s.append("object");
    }
    if (isArray) {
      s.append("Array");
    }
    return s.toString();
  }

  public String generateCType() {
    String type;
    if (!isPrimitive) {
      type = mangleIdentifier(this.type);
    } else {
      if (this.type.equals("byte"))
        type = "int8_t";
      else if (this.type.equals("boolean"))
        type = "uint8_t";
      else if (this.type.equals("char"))
        type = "uint16_t";
      else if (this.type.equals("short"))
        type = "int16_t";
      else if (this.type.equals("int"))
        type = "int32_t";
      else if (this.type.equals("long"))
        type = "int64_t";
      else if (this.type.equals("float"))
        type = "float";
      else if (this.type.equals("double"))
        type = "double";
      else if (this.type.equals("void"))
        type = "void";
      else
        type = "UNKNOWN";
    }
    return type;
  }

  public boolean isPrimitiveType() {
    return isPrimitive;
  }

  /** Make Java identifiers (a.b.c.Delta) C safe (a_b_c_Delta) */
  public static String mangleIdentifier(String id) {
    return id.replace(".", "_");
  }

  public String generateJSignature() {
    String s;

    if (this.type.equals("boolean"))
      s = "Z";
    else if (this.type.equals("byte"))
      s = "B";
    else if (this.type.equals("char"))
      s = "C";
    else if (this.type.equals("short"))
      s = "S";
    else if (this.type.equals("int"))
      s = "I";
    else if (this.type.equals("long"))
      s = "J";
    else if (this.type.equals("float"))
      s = "F";
    else if (this.type.equals("double"))
      s = "D";
    else if (this.type.equals("void"))
      s = "V";
    else
      s = "L" + this.qualifiedType + ";";
    
    if (this.isArray) {
      return "[" + s;
    } else {
      return s;
    }
  }
}