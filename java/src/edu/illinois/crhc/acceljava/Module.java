package edu.illinois.crhc.acceljava;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sun.source.tree.Tree;
import com.sun.source.tree.VariableTree;


public class Module {
  public String name; // a.b.c.Foo.BarInner
  public String qualifiedType; // a/b/c/Foo$BarInner
  public String pack, scope;
  public HashMap<String, ClassMirror> classMirrors = new HashMap<String, ClassMirror>();
  public List<String> libs = new ArrayList<String>();
  public List<String> headers = new ArrayList<String>();
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<Function> functions = new ArrayList<Function>();

  public ArrayList<Function> getFunctions() {
    return functions;
  }

  public void setFunctions(ArrayList<Function> functions) {
    this.functions = functions;
  }

  public int lineNumberStart;

  public void addClassMirror(ClassLookup lookup, String identifier) {
    ClassMirror classMirror = new ClassMirror();
    classMirror.name = FunctionArg.mangleIdentifier(identifier);
    if (classMirrors.containsKey(classMirror.name)) {
      return;
    }
    for (Tree t : lookup.tree.getMembers()) {
      if (t.getKind() == Tree.Kind.VARIABLE) {
        VariableTree varTree = (VariableTree) t;
        FunctionArg member = new FunctionArg(varTree);
        // Only allow primitive members to be replicated
        if (member.isPrimitiveType() && !member.isArray()) {
          classMirror.members.add(member);
        } else {
          System.out.println("WARNING: Cannot mirror member " + identifier + "."
              + varTree.getName() + " - can only mirror primitive class members.");
        }
      }
    }
    classMirror.qualifiedType = lookup.qualifiedType;
    classMirrors.put(classMirror.name, classMirror);
  }
  
  public List<String> getHeaders() {
    return headers;
  }
  
  public List<String> getLibs() {
    return libs;
  }
}