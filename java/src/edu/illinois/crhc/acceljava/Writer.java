package edu.illinois.crhc.acceljava;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public class Writer {
  public static final String GENERATED_HEADER = "AccelJava Generated File, DO NOT MODIFY";
  
  static {
    Properties p = new Properties();
    p.setProperty("resource.loader", "class");
    p.setProperty("class.resource.loader.class",
        "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
    Velocity.init(p);
  }

  public static void writeJni(List<Module> modules, String jniPath) {
    for (Module m : modules) {
      VelocityContext context = new VelocityContext();
      context.put("classIdentifier", m.qualifiedType);
      context.put("className", m.name);
      context.put("methods", m.functions);
      context.put("classes", m.classMirrors.values());
      context.put("headers", m.getHeaders());

      FileWriter writer;
      try {
        writer = new FileWriter(jniPath + m.name + ".c");

        Velocity.getTemplate("edu/illinois/crhc/acceljava/writer/nativeModule.c").merge(context,
            writer);
        writer.close();

      } catch (IOException e) {
        System.err.println("Error writing output files.");
        e.printStackTrace();
      }
    }
  }

  public static void writeAndroidMk(List<Module> modules, String androidMk, String scriptDir) {
    VelocityContext context = new VelocityContext();
    context.put("gen_header", GENERATED_HEADER);
    context.put("script_dir", scriptDir);
    context.put("modules", modules);

    FileWriter writer;
    try {
      writer = new FileWriter(androidMk);

      Velocity.getTemplate("edu/illinois/crhc/acceljava/writer/androidMake.mk").merge(context,
          writer);
      writer.close();

    } catch (IOException e) {
      System.err.println("Error writing output files.");
      e.printStackTrace();
    }
  }

  /** Write out library include compiler line to external file */
  public static void writeExternals(List<Module> modules, String jniPath) {
    for (Module m : modules) {
      try {
        FileWriter makeLibs = new FileWriter(jniPath + m.name + ".so.libs");
        for (String lib : m.getLibs()) {
          makeLibs.write("-l" + lib + " ");
        }
        makeLibs.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
