package edu.illinois.crhc.acceljava;

import java.util.ArrayList;


/**
 * CudaKernels token.
 */
public class ClassMirror {
  public String name;
  public String qualifiedType;
  public ArrayList<FunctionArg> members = new ArrayList<FunctionArg>();

  public ArrayList<FunctionArg> getMembers() {
    return members;
  }

  public String getName() {
    return name;
  }

  public String getQualifiedType() {
    return qualifiedType;
  }
}