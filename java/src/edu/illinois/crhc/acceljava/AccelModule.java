package edu.illinois.crhc.acceljava;

import java.util.HashMap;
import java.util.Map;

/**
 * Marks a class as an accelerated module.
 */
public class AccelModule {

  static final Map<String, Boolean> __loadedClasses = new HashMap<String, Boolean>();
  
  public int getBlockDim() {
    return blockDim;
  }

  public void setBlockDim(int blockDim) {
    this.blockDim = blockDim;
  }

  public int getThreadDim() {
    return threadDim;
  }

  public void setThreadDim(int threadDim) {
    this.threadDim = threadDim;
  }

  private int blockDim, threadDim;
  
  public AccelModule() {
    this(0,0);
  }
  
  public AccelModule(int blockDim, int threadDim) {
    String fullName = getClass().getCanonicalName();
    if (!__loadedClasses.containsKey(fullName)) {
      System.loadLibrary(fullName);
      __loadedClasses.put(fullName, true);
    }
    
    this.blockDim = blockDim;
    this.threadDim = threadDim;
  }
  
}
