package com.example.android.skeletonapp;

import edu.illinois.crhc.acceljava.AccelModule;

public class NativeFunctions extends AccelModule {

  public NativeFunctions(int blockDim, int threadDim) {
    super(blockDim, threadDim);
  }
  
  public native int add(int a, int b); /*-{
    return a + b;
  }-*/
  
  public native int subtract(int a, int b); /*-{
    return a - b;
  }-*/
}
