Ian Wetherbee
University of Illinois: Urbana-Champaign
IMPACT Research Group

AccelJava
========================

Build/Install:
------------------------
See INSTALL.txt


Example project:
------------------------
rootdir/src - java source
rootdir/bin - output shared libraries and compiled classes
rootdir/src/com/example/www/Main.java - main class file


Run compiler:
------------------------
The AccelJava compiler takes a [project directory and main class file] and outputs [shared library 
objects and generated source files]. 

./acceljava.sh -m <mode> [-o <output directory>] [-p <classpath>] <project root> <main class source file>
./acceljava.sh -m native ./rootdir com/example/www/Main.java


Compile generated source:
------------------------
Launch the javac compiler on the main class source file in the src/ directory.

cd rootdir/
javac -sourcepath src/ -cp /acceljava/dist/AccelJava-0.1.jar -d bin/ src/com/example/www/Main.java


Android mode:
------------------------
Source files will be output to jni/, but not compiled. Instead, a file named jni/Android.mk will be
created if none already exists, else jni/AccelJava.android.mk that can be included in the 
"Android.mk" file with the line "include AccelJava.android.mk".

An example workflow:
./acceljava.sh -m android ./rootdir com/example/www/Main.java
cd rootdir/
$NDK/ndk-build


Run project:
------------------------
Run your compiled project by including the AccelJava runtime jar and configuring your library.path
to load the generated shared libraries in bin/.

java -Djava.library.path=rootdir/bin/ -classpath /acceljava/dist/AccelJava-0.1.jar:rootdir/bin/ com.example.www.Main