# Options
#
# -m mode
# -c C compiler-executable
# -f C compiler flags
# -p java compiler classpath
# -o output/bin directory
# $acceljava -m [native|android] (options) (--) [java-root] [main-class]

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
JAR="$SCRIPT_DIR/dist/AccelJava-0.1.jar"

ARGP=$(getopt -n AccelJava -o m:c:f:o:p: --long mode:,compiler:,flags:,output:,classpath: -- "$@")

[ $? = 0 ] || exit 1;
COMPILER=gcc
FLAGS=-Wall
CLASSPATH=""

eval set -- "$ARGP"
while true; do
  case "$1" in
    -m|--mode)
      MODE="$2"; shift; shift; continue
    ;;
    -c|--compiler)
      COMPILER="$2"; shift; shift; continue
    ;;
    -f|--flags)
      FLAGS="$2"; shift; shift; continue
    ;;
    -o|--output)
      OUTPUTDIR="$2"; shift; shift; continue
    ;;
    -p|--classpath)
      CLASSPATH="$2"; shift; shift; continue
    ;;
    --)
      shift; break
    ;;
    *)
      printf "Bad option %s %s\n" "$1"
      exit 1
    ;;
  esac
done

[ -z $MODE ] && echo "Mode (-m) flag required" && exit 1

eval set -- "$@"
JAVAROOT=$1
MAINCLASS=$2
CLASSPATH=$JAR:$CLASSPATH
[ -z $OUTPUTDIR ] && OUTPUTDIR=$JAVAROOT/bin

echo "$SCRIPT_DIR mode=$MODE compiler=$COMPILER flags=$FLAGS javaroot=$JAVAROOT"

# Parse out inline kernels to jni/
[ -d $JAVAROOT/jni ] || mkdir $JAVAROOT/jni
java -classpath $JAR edu.illinois.crhc.acceljava.Main $MODE $JAVAROOT $SCRIPT_DIR

if [ "$MODE" = "native" ]
then
  # Compile kernels and link with chosen runtime library
  make -f "$SCRIPT_DIR/KernelCompile.mk" $MODE JNIPATH=$JAVAROOT/jni/ MODE=$MODE CC=$COMPILER CFLAGS=$FLAGS SCRIPT_DIR=$SCRIPT_DIR
  
  # Copy the shared libraries to the final location
  [ -d $OUTPUTDIR ] || mkdir -p $OUTPUTDIR
  for so in $JAVAROOT/jni/*.so
  do
  	mv $so $OUTPUTDIR/lib$(basename $so)
  done
else
  [ -d $JAVAROOT/libs ] || mkdir $JAVAROOT/libs
  cp $JAR $JAVAROOT/libs/
fi
