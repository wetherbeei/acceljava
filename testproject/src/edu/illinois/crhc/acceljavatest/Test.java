package edu.illinois.crhc.acceljavatest;

import edu.illinois.crhc.acceljava.AccelModule;
import edu.illinois.crhc.acceljava.Include;
import edu.illinois.crhc.acceljavatest.sub.Data2;

public class Test {
  static class Data3 {
    int foo;
  }
  
  @Include(libs={"m", 
      "z"}, headers={"math.h"})
  public static class Inner 
      extends AccelModule {
    
    public Inner() {
      super();
    }
    
    // Object arguments are passed as pointers
    public native void identity(final Data3 a, Data2 b); /*-{
      b->bar = a->foo;
    }-*/
    
    public native int returnPrimitiveTest(); /*-{
      return 1234;
    }-*/
    
    public native Data3 returnClassTest(); /*-{
      Data3 d;
      d.foo = 1234;
      return d;
    }-*/
    
    public native int passthroughClassTest(Data3 a); /*-{
      return a->foo;
    }-*/
    
    public native int privateFunctionCallTest(); /*-{
      return privateFunctionCall();
    }-*/
    
    private native int privateFunctionCall(); /*-{
      return 1234;
    }-*/
    
    public native int arraySumTest(final int[] array, final int n); /*-{
      int tmp = 0, i;
      for (i = 0; i < n; i++) {
        tmp += array[i];
      }
      return tmp;
    }-*/
    
    public native void arrayCopyTest(final int[] inArray, int[] outArray, final int n); /*-{
      int i;
      for (i = 0; i < n; i++) {
        outArray[i] = inArray[i];
      }
    }-*/
    
    public native int arrayObjectSumTest(final Data1[] array, final int n); /*-{
      int tmp = 0, i;
      for (i = 0; i < n; i++) {
        tmp += array[i].foo;
      }
      return tmp;
    }-*/
    
    public native float libMathTest(float input); /*-{
      return sqrtf(input);
    }-*/
  }
}
