package edu.illinois.crhc.acceljavatest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.*;

import edu.illinois.crhc.acceljavatest.Test.Data3;
import edu.illinois.crhc.acceljavatest.Test.Inner;
import edu.illinois.crhc.acceljavatest.sub.Data2;

public class Main {

  /**
   * @param args
   */
  public static void main(String[] args) {
    Result result = JUnitCore.runClasses(Main.class);
    System.out.println("Tests run: " + result.getRunCount() + ", " 
        + result.getFailureCount() + " failed");
  }
  
  @Test
  public void identity() {
    Data3 a = new Data3();
    a.foo = 1234;
    Data2 b = new Data2();
    new Inner().identity(a, b);
    assertEquals(a.foo, b.bar);
  }
  
  @Test
  public void primitiveReturn() {
    int retTest = new Inner().returnPrimitiveTest();
    assertEquals(retTest, 1234);
  }
  
  @Test
  public void classReturn() {
    int retTest = new Inner().returnClassTest().foo;
    assertEquals(retTest, 1234);
  }
  
  @Test
  public void passthroughClass() {
    Data3 a = new Data3();
    a.foo = 2;
    int retTest = new Inner().passthroughClassTest(a);
    assertEquals(a.foo, retTest);
  }
  
  @Test
  public void privateFunction() {
    int retTest = new Inner().privateFunctionCallTest();
    assertEquals(retTest, 1234);
  }
  
  @Test
  public void array() {
    int retTest;
    int sumArray[] = {1,2,3,4};
    retTest = new Inner().arraySumTest(sumArray, sumArray.length);
    int localSum = 0;
    for (int i = 0; i < sumArray.length; i++) {
      localSum += sumArray[i];
    }
    assertEquals(localSum, retTest);
    
    int outArray[] = {0,0,0,0}; // Alloc space
    new Inner().arrayCopyTest(sumArray, outArray, sumArray.length);
    boolean pass = true;
    for (int i = 0; i < sumArray.length; i++) {
      if (sumArray[i] != outArray[i]) {
        pass = false;
      }
    }
    assertTrue(pass);
  }
  
  @Test
  public void classArray() {
    int retTest;
    Data1 x = new Data1(), y = new Data1(), z = new Data1();
    x.foo = 1;
    y.foo = 2;
    z.foo = 3;
    Data1 objArray[] = {x,y,z};
    retTest = new Inner().arrayObjectSumTest(objArray, objArray.length);
    int localSum = 0;
    for (int i = 0; i < objArray.length; i++) {
      localSum += objArray[i].foo;
    }
    assertEquals(localSum, retTest);
  }
  
  @Test
  public void libMath() {
    float testFloat = 25;
    float retFloat = new Inner().libMathTest(testFloat);
    assertTrue(retFloat == Math.sqrt(testFloat));
  }
}
